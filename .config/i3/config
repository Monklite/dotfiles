# My i3-gaps Configuration File

# Execs
exec --no-startup-id hsetroot -fill /home/dave/Pictures/Wallpapers/Misc/wallhaven-6kgdpx.png
exec --no-startup-id xinput set-prop 12 "libinput Accel Speed" -0.5
exec --no-startup-id xautolock -time 10 -corners 000- -detectsleep -locker "~/.config/i3/betterlockscreen/betterlockscreen -l dimblur"
exec --no-startup-id picom
exec --no-startup-id dunst -config /home/dave/.config/dunst/dunstrc

# General settings
set $mod Mod4
font pango:Droid Sans 8
#font pango:DejaVu Sans Mono 8
floating_modifier $mod
set $ws1 "1:  "
set $ws2 "2:  "
set $ws3 "3: "
set $ws4 "4"
set $ws5 "5"
set $ws6 "6"
set $ws7 "7"
set $ws8 "8"
set $ws9 "9"
set $ws10 "10"
# Gaps settings
gaps inner 6
gaps outer 0
default_border pixel 1
focus_follows_mouse no 
title_align center

# Keybinds
bindsym $mod+Return exec --no-startup-id i3-sensible-terminal
bindsym $mod+p exec --no-startup-id st -n scratchpad
bindsym $mod+Shift+slash kill
bindsym $mod+d exec --no-startup-id dmenu_run -p "Run:"
bindsym Print exec --no-startup-id "scrot -e 'mv $f ~/Pictures/Screenshots'"
bindsym --release Caps_Lock exec pkill -SIGRTMIN+12 i3blocks

# Scratchpad
for_window [instance="scratchpad"] floating enable

# Shutdown/reboot confirmation scripts
bindsym $mod+Delete exec --no-startup-id ~/.config/scripts/prompt.sh
bindsym $mod+Home exec --no-startup-id ~/.config/scripts/monitor.sh
bindsym $mod+End exec  --no-startup-id ~/.config/scripts/selScript.sh
bindsym $mod+Shift+g exec --no-startup-id ~/.config/scripts/gapsToggle.sh

# change focus
bindsym $mod+h focus left
bindsym $mod+j focus down
bindsym $mod+k focus up
bindsym $mod+l focus right

# change focus (cursor keys)
bindsym $mod+Left focus left
bindsym $mod+Down focus down
bindsym $mod+Up focus up
bindsym $mod+Right focus right

# move focused window
bindsym $mod+Shift+h move left
bindsym $mod+Shift+j move down
bindsym $mod+Shift+k move up
bindsym $mod+Shift+l move right

# move focused window (cursor keys):
bindsym $mod+Shift+Left move left
bindsym $mod+Shift+Down move down
bindsym $mod+Shift+Up move up
bindsym $mod+Shift+Right move right

# split in horizontal orientation
bindsym $mod+b split h

# split in vertical orientation
bindsym $mod+v split v

# enter fullscreen mode for the focused container
bindsym $mod+f fullscreen toggle

# change container layout (stacked, tabbed, toggle split)
bindsym $mod+s layout stacking
bindsym $mod+w layout tabbed
bindsym $mod+e layout toggle split

# toggle tiling / floating
bindsym $mod+Shift+space floating toggle

# change focus between tiling / floating windows
bindsym $mod+space focus mode_toggle

# focus the parent container
bindsym $mod+a focus parent
# focus the child container
#bindsym $mod+d focus child

# switch to workspace
bindsym $mod+1 workspace $ws1
bindsym $mod+2 workspace $ws2
bindsym $mod+3 workspace $ws3
bindsym $mod+4 workspace $ws4
bindsym $mod+5 workspace $ws5
bindsym $mod+6 workspace $ws6
bindsym $mod+7 workspace $ws7
bindsym $mod+8 workspace $ws8
bindsym $mod+9 workspace $ws9
bindsym $mod+0 workspace $ws10
bindsym $mod+minus workspace prev
bindsym $mod+equal workspace next

# move focused container to workspace
bindsym $mod+Shift+1 move container to workspace $ws1
bindsym $mod+Shift+2 move container to workspace $ws2
bindsym $mod+Shift+3 move container to workspace $ws3
bindsym $mod+Shift+4 move container to workspace $ws4
bindsym $mod+Shift+5 move container to workspace $ws5
bindsym $mod+Shift+6 move container to workspace $ws6
bindsym $mod+Shift+7 move container to workspace $ws7
bindsym $mod+Shift+8 move container to workspace $ws8
bindsym $mod+Shift+9 move container to workspace $ws9
bindsym $mod+Shift+0 move container to workspace $ws10

# resize hotkeys
bindsym $mod+Shift+y	resize shrink width 10 px or 10 ppt
bindsym $mod+Shift+u	resize shrink height 10 px or 10 ppt
bindsym $mod+Shift+i	resize grow height 10 px or 10 ppt
bindsym $mod+Shift+o	resize grow width 10 px or 10 ppt

# reload the configuration file
bindsym $mod+Shift+c reload
# restart i3 inplace (preserves your layout/session, can be used to upgrade i3)
bindsym $mod+Shift+r restart
# exit i3 (logs you out of your X session)
bindsym $mod+Shift+e exec "i3-nagbar -t warning -m 'You pressed the exit shortcut. Do you really want to exit i3? This will end your X session.' -b 'Yes, exit i3' 'i3-msg exit'"

bar {
		status_command "i3blocks -c ~/.config/i3blocks/i3blocks.conf"
		i3bar_command i3bar -t
		position top
		mode dock
		modifier None
		font pango:Droid Sans, Font Awesome 5 Free 8
		colors{
			background #282c34
			focused_workspace  #FFFFFF  #2a2a2b      #FFFFFF
			active_workspace   #737373  #2a2a2b      #FFFFFF
			inactive_workspace #737373  #2a2a2b      #FFFFFF	
		}
}

# colors                border  backgr. text    indicator child_border
client.focused          #ffffff #424242 #ffffff #ffffff   #ffffff
client.focused_inactive #333333 #424242 #ffffff #484e50   #5f676a
client.unfocused        #333333 #222222 #888888 #292d2e   #222222
client.urgent           #2f343a #900000 #ffffff #900000   #900000
client.placeholder      #000000 #0c0c0c #ffffff #000000   #0c0c0c
client.background       #ffffff

# PulseAudio Controller
bindsym XF86AudioRaiseVolume exec --no-startup-id pactl set-sink-volume 0 +5%; exec --no-startup-id pkill -RTMIN+10 i3blocks
bindsym XF86AudioLowerVolume exec --no-startup-id pactl set-sink-volume 0 -5%; exec --no-startup-id pkill -RTMIN+10 i3blocks
bindsym XF86AudioMute exec --no-startup-id pactl set-sink-mute 0 toggle; exec --no-startup-id pkill -RTMIN+10 i3blocks
bindsym XF86AudioMicMute exec --no-startup-id pactl set-source-mute 1 toggle; exec --no-startup-id pkill -RTMIN+10 i3blocks

#Screen Brightness
bindsym XF86MonBrightnessUp exec light -A 10; exec --no-startup-id pkill -RTMIN+11 i3blocks
bindsym XF86MonBrightnessDown exec light -U 10; exec --no-startup-id pkill -RTMIN+11 i3blocks

#Media player controls
bindsym XF86Search exec --no-startup-id playerctl play-pause
bindsym XF86Explorer exec --no-startup-id playerctl next
bindsym XF86Tools exec --no-startup-id playerctl previous
bindsym XF86LaunchA exec --no-startup-id playerctl stop
