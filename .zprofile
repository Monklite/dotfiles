#
# ~/.zprofile
#

# Defaults
export EDITOR="nvim"
export TERMINAL="st"
export BROWSER="firefox"
export READER="zathura"
export PATH="$PATH:$(du "$HOME/.local/bin/" | cut -f2 | paste -sd ':')"

# Config centralization
export XDG_CONFIG_HOME="$HOME/.config"
export XDG_DATA_HOME="$HOME/.local/share"
export XDG_CACHE_HOME="$HOME/.cache"
export GTK2_RC_FILES="${XDG_CONFIG_HOME:-$HOME/.config}/gtk-2.0/gtkrc-2.0"
export INPUTRC="${XDG_CONFIG_HOME:-$HOME/.config}/inputrc"
export ZDOTDIR="${XDG_CONFIG_HOME:-$HOME/.config}/zsh"

# Misc
export QT_QPA_PLATFORMTHEME=gtk2
export FZF_DEFAULT_OPTS=$FZF_DEFAULT_OPTS'
 --color=fg:#abb2bf,hl:#61afef
 --color=fg+:#abb2bf,bg+:#353b45,hl+:#56b6c2
 --color=info:#afaf87,prompt:#e06c75,pointer:#c678dd
 --color=marker:#98c379,spinner:#c678dd,header:#e5c07b'

# Window Manager
[[ -f ~/.zshrc ]] && . ~/.zshrc

if [[ ! $DISPLAY && $XDG_VTNR -eq 1 ]]; then
	exec startx
fi
